var express = require('express');
var fs = require('fs');
var cors = require('cors');
var https = require('https');
var Keycloak = require('keycloak-connect');
var session = require('express-session');
var app = express();
var morgan = require('morgan')
app.use(morgan('combined'))
/*
var allowedOrigins = ['https://s2.videostream.ninja:9001',
                      'https://s2.videostream.ninja:9002',
		      'https://auth.videostream.ninja:8443'];
*/
/*
var corsOptions = {
  origin: 'https://s2.videostream.ninja:9001',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors({
    origin: 'https://s2.videostream.ninja:9002'
}));
*/
/*
app.use(cors({
  origin: function(origin, callback){
    // allow requests with no origin
    // (like mobile apps or curl requests)
    if(!origin) return callback(null, true);
    if(allowedOrigins.indexOf(origin) === -1){
      var msg = 'The CORS policy for this site does not ' +
                'allow access from the specified Origin.';
      return callback(new Error(msg), false);
    }
    return callback(null, true);
  },
  optionsSuccessStatus: 200, 
  Content-Type: 'application/x-www-form-urlencoded',
}));
*/

var corsOptions = {
  "origin": ['https://s2.videostream.ninja:9001', 'https://s2.videostream.ninja:9002', 'https://auth.videostream.ninja:8443'],
  "optionsSuccessStatus": "200", 
  "methods": "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
//  "preflightContinue" : "false",
//  "credentials" : "true",
  "allowedHeaders" : "['Content-Type', 'Authorization']"
}
app.options('*', cors(corsOptions))
var memoryStore = new session.MemoryStore();

app.use(session({
    secret: 'some secret',
    resave: false,
    saveUninitialized: true,
    store: memoryStore
}));

var ckConfig = {
    "realm": "cycle",
    "clientId": "reactapp",
    "auth-server-url": "https://auth.videostream.ninja:8443/auth/",
    "ssl-required": "external",
    "resource": "reactapp",
    "public-client": true,
    "verify-token-audience": true,
    "use-resource-role-mappings": true,
    "confidential-port": 0,
    "enable-cors": true
};

var keycloak = new Keycloak({ store: memoryStore }, ckConfig);
app.use(keycloak.middleware());
app.all('*', keycloak.protect());

app.get("/fruit", (req, res, next) => {
    res.json(["Apple", "Pear", "Grape", "Orange", "Banana"]);
});

https.createServer({
    key: fs.readFileSync('ssl/privkey1.pem'),
    cert: fs.readFileSync('ssl/fullchain1.pem')
}, app)
    .listen(9001, '0.0.0.0', function () {
        console.log('Example app listening on port 3000! Go to https://localhost:3000/')
    })


// var server = app.listen(9001, 's2.videostream.ninja', function () {

//     var host = server.address().address
//     var port = server.address().port

//     console.log("Example app listening at http://%s:%s", host, port)
// })
